import "jquery";
import {BasePage} from "../base-page";
import "slick-carousel/slick/slick.less";
import "slick-carousel/slick/slick";
import "smartscroll/smartscroll";

class Index extends BasePage {
    init() {
        this.initGalery();
        this.initScroll();
    }

    initGalery() {
        const slider = this.$element.find('.js-slider').first();
        slider.slick({
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3
                }
            }, {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            }, {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1
                }
            }],
            infinite: false,
            prevArrow: '.js-production-prev',
            nextArrow: '.js-production-next',
            slidesToShow: 4
        });
    }

    initScroll() {
        let options = {
            mode: "set",
            autoHash: false,
            sectionWrapperSelector: ".js-section-wrapper",
            sectionClass: "section",
            animationSpeed:400,
            headerHash: "header",
            breakpoint: null,
            dynamicHeight: false,
            bindSwipe: false,
        };
        $.smartscroll(options);
    }
}

const page = new Index();